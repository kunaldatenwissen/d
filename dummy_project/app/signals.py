from django.db.models.signals import post_save, pre_save,post_delete,pre_delete

from django.dispatch import receiver

from .models import *

import datetime

import logging
log = logging.getLogger('django')

@receiver(post_save,sender=User)
def user_histroy(sender,instance,created,**kwargs):
    if created:
        print('---------',instance)
        log.info(msg=''+str(instance.first_name)+' '+str(instance.last_name)+' Registered')
        UserHistory.objects.create(user=instance,Datatype='reg',date_time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") )
    