from django.db import models
from django.contrib.auth.models import User
import datetime
# Create your models here.


class UserHistory(models.Model):
    Datatype_CHOICES = [
        ('reg', 'Register'),
        ('li', 'Login'),
        ('lo', 'Logout'),
    ]
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    Datatype=models.CharField(max_length=5,choices=Datatype_CHOICES)
    date_time=models.DateTimeField()
    def __str__(self):
        return self.Datatype