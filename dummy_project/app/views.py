from django.shortcuts import render,redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.hashers import make_password
import datetime

from app.models import UserHistory

import logging
logger = logging.getLogger('django')

# Create your views here.

def register_page(request):
    if request.method == 'POST':
        email=request.POST.get('email')
        first_name=request.POST.get('first_name')
        last_name=request.POST.get('last_name')
        password=request.POST.get('password')
        confirm_password=request.POST.get('confirm_password')
        print(email,first_name,last_name,password,confirm_password)
        if email and first_name and last_name and password and confirm_password:
            if password == confirm_password:
                hashed_password=make_password(password)
                reg_user=User(username=email,email=email,first_name=first_name,last_name=last_name,password=hashed_password)
                reg_user.save()
    return render(request,'Register_page.html')




def login_page(request):
    if request.method == 'POST':
        email=request.POST.get('email')
        password=request.POST.get('password')
        print(email,password)
        if email and password:
            user=authenticate(username=email,password=password)
            login(request,user)

        if request.user.is_authenticated:
            logger.info(msg=''+str(request.user.first_name)+' '+str(request.user.last_name)+' logged In')
            user_login=UserHistory(user=request.user,Datatype='li',date_time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") )
            print(user_login)
            user_login.save()
            return redirect('index_page')
    return render(request,'Login.html')



def index_page(request):
    obj_list=[]
    if request.user.is_authenticated:
        print(request.user)
        obj={
            'first_name':request.user.first_name,
            'last_name':request.user.last_name,
        }
        obj_list.append(obj)
    context={
        'user':obj_list
    }
    print(context)
    return render(request,'Index.html',context)



def logout_page(request):
    if request.user.is_authenticated:
        logger.info(msg=''+str(request.user.first_name)+' '+str(request.user.last_name)+' logged out')
        user_logout=UserHistory(user=request.user,Datatype='lo',date_time=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") )
        print(user_logout)
        user_logout.save()
        logout(request)
        return redirect('login_page')